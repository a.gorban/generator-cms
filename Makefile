#!/usr/bin/make

REGISTRY_HOST = registry.gitlab.com
REGISTRY_PATH = mbkkong/generator-cms

PUBLISH_TAGS = latest
PULL_TAG = latest

APP_IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)/php
APP_IMAGE_LOCAL_TAG = $(IMAGES_PREFIX)_php
APP_IMAGE_DOCKERFILE = ./docker/php/Dockerfile
APP_IMAGE_CONTEXT = ./docker/php

APP_DEV_IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)/php_dev
APP_DEV_IMAGE_LOCAL_TAG = $(IMAGES_PREFIX)_php_dev
APP_DEV_IMAGE_DOCKERFILE = ./docker/php_dev/Dockerfile
APP_DEV_IMAGE_CONTEXT = ./docker/php_dev

APP_CONTAINER_NAME := app

docker_bin := $(shell command -v docker 2> /dev/null)
docker_compose_bin := $(shell command -v docker-compose 2> /dev/null)

# =====================================================================================================================
# //	APPLICATION
# =====================================================================================================================
app_build: ## Application - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --cache-from "$(APP_IMAGE):$(PULL_TAG)" \
	  $(foreach tag_name,$(PUBLISH_TAGS),--tag "$(APP_IMAGE):$(tag_name)") \
	  -f $(APP_IMAGE_DOCKERFILE) $(APP_IMAGE_CONTEXT);
	$(foreach tag_name,$(PUBLISH_TAGS),$(docker_bin) push "$(APP_IMAGE):$(tag_name)";)

app_dev_build: ## Application Development - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --cache-from "$(APP_DEV_IMAGE):$(PULL_TAG)" \
	  $(foreach tag_name,$(PUBLISH_TAGS),--tag "$(APP_DEV_IMAGE):$(tag_name)") \
	  -f $(APP_DEV_IMAGE_DOCKERFILE) $(APP_DEV_IMAGE_CONTEXT);
	$(foreach tag_name,$(PUBLISH_TAGS),$(docker_bin) push "$(APP_DEV_IMAGE):$(tag_name)";)

# =====================================================================================================================
# //	DEVELOPMENT TASKS
# =====================================================================================================================
up: ## Clear volumes and start all containers (in background) for development
	$(docker_compose_bin) -f docker-compose.yml up --build -d

up_dev: ## Dev mode
	$(docker_compose_bin) -f docker-compose.dev.yml up --build -d

clear_up:
	- rm -rf app/node_modules
	- rm -rf app/vendor
	- rm app/.env
	- rm -rf /monitoring/node_modules
	$(docker_compose_bin) up -d --build

clear_up_dev:
	- rm -rf app/node_modules
	- rm -rf app/vendor
	- rm app/.env
	- rm -rf /monitoring/node_modules
	$(docker_compose_bin) -f docker-compose.dev.yml up --build -d

down: ## Stop all started for development containers
	$(docker_compose_bin) down

restart: ## Restart all started for development containers
	$(docker_compose_bin) restart

shell: ## Start shell into application container
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" /bin/sh

install: ## Install application dependencies into application container
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" composer install --no-interaction --ansi --no-suggest
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" yarn install --network-timeout 1000000000

watch: ## Start watching assets for changes (node)
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" yarn watch

migrate: ## Start watching assets for changes (node)
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan migrate --force --no-interaction -vvv

seed: ## Start watching assets for changes (node)
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan db:seed

key_generate: ## Generate key for laravel app
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan key:generate
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" composer dump-autoload
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan config:clear

optimize: ## Optimize app
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan optimize
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan config:clear

init: ## Make full application initialization (install, seed, build assets, etc)
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan migrate --force --no-interaction -vvv
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" php artisan db:seed --force -vvv
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" yarn run dev

build:
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" yarn run dev

production:
	$(docker_compose_bin) exec "$(APP_CONTAINER_NAME)" yarn run production
