<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        $howMach = 2000;
        $howMachUsers = 5;
        foreach (range(1, $howMach) as $index) {
            DB::table('posts')->insert([
                'title' => $faker->unique()->words($nb=3, $asText=true),
                'text' => $faker->text($maxNbChars = 2000, $indexSize = 5),
                'slug' => $faker->unique()->slug,
                'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
                'h1' => $faker->words($nb=4, $asText=true),
                'user_id' => $faker->numberBetween($min = 1, $max = $howMachUsers),
                'published_at' => $faker->iso8601($max = 'now'),
            ]);
        }
    }
}
