<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class UploadController extends Controller
{
    private $directory;

    public function __construct()
    {
        $this->directory = 'public/posts';
    }

    public function getForm()
    {
        return view('upload-form');
    }

    public function upload(Request $request)
    {
        // Берём объект файл
        $path = $request->file('file');

        // Мы берем расширение файла (jpg, png, gif)
        $extension = $path->getClientOriginalExtension();

        if($extension === "jpg") {
            // Создаем название для файла (1.jpg, 2.jpg)
            $name = $path->getClientOriginalName();
            // Сохраняем файл на диске (FTP, Local)
            Storage::disk('local')->putFileAs($this->directory . $request['folderName'], new File($path), $name);
            // Возвращаем название файла чтобы выводит на FrontEnd
            return "Успех! Файл " . $name . " загружен";
        }else{
            return "Ошибка! Выберите изображение с расширением .jpg";
        }
    }

}