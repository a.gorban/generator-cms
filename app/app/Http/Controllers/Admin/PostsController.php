<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PostRequest;
use Carbon\Carbon;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'asc')->paginate(20);

        return view('admin.posts.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $dateNow = Carbon::now();
        return view('admin.posts.create', [
            'categories' => $categories,
            'dateNow' => $dateNow
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
//        dd($request);
        if ($request->input('publish_now')) {
            $published_at = Carbon::now();
            } else $published_at = $request->input('published_at');
        $newPost = new Post([
            'title' => $request->input('title'),
            'text' => $request->input('text'),
            'slug' => str_slug($request->input('title', '-')),
            'description' => $request->input('description'),
            'h1' => $request->input('h1'),
            'user_id' => Auth::user()->id,
            'published_at' => $published_at,
        ]);
        $newPost->save();
        return redirect(route('admin.posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('id', $id)->with('categories')->first();
        $categories = Category::all();
        return view('admin.posts.edit', [
            'categories' => $categories,
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);
        $post->h1 = $request->input('h1');
        $post->title = $request->input('title');
        $post->slug = str_slug($request->input('title', '-'));
        $post->description = $request->input('description');
        $post->text = $request->input('text');
        $post->published_at = $request->input('published_at');
        $categoriesIds = $request->input('parent_id');
        $post->categories()->detach();
        $post->categories()->attach($categoriesIds);
        $post->save();
        return redirect(route('admin.posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->categories()->detach();
        $post->delete();
        return redirect(route('admin.posts'));
    }
}
