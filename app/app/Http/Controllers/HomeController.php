<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Category;
use Carbon\Carbon;


class HomeController extends Controller
{
    public function index()
    {
        $random_posts = Post::inRandomOrder()->limit(5)->get();
        $categories   = Category::with('children')->where('parent_id', 0)->get();
        $posts        = Post::where('published_at', '<=', Carbon::now())->orderBy('published_at', 'desc')->paginate(20);
        return view('home', [
            'categories'   => $categories,
            'posts'        => $posts,
            'random_posts' => $random_posts,
            'settings'     => $this->settings() // Возварашаем функцию настроек с Controller, от куда наследуемся
        ]);
    }
}
