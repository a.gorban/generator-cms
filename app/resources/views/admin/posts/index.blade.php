@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-2">
            <div class="col d-flex align-items-center justify-content-between">
                <h3>Посты</h3>
                <a href="{{route('admin.posts.create')}}">
                    <button class="btn btn-success">Создать пост</button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Админстратор</th>
                        <th scope="col">Дата публукации</th>
                        <th scope="col" class="text-center">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($posts as $post)
                        <tr>
                            <th scope="row">{{ $post->id }}</th>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->user->name }}</td>
                            <td>{{ $post->published_at }}</td>
                            <td class="d-flex justify-content-center">
                                <a href="{{ route('admin.posts.edit', ["id"=>$post->id]) }}" class="ml-1 mr-1">
                                    <button type="button" class="btn btn-primary btn-sm">Редактировать</button>
                                </a>
                                <form action="{{ route('admin.posts.delete', ['id'=>$post->id]) }}" method='POST'
                                      class="ml-1 mr-1">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        sdg
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col">
                {{ $posts->links() }}
            </div>
        </div>

    </div>
@endsection
