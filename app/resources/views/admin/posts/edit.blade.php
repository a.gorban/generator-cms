@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.posts.update', ["id"=>$post->id]) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="h1">H1</label>
                        <input type="text" class="form-control" id="h1" name="h1" placeholder="H1"
                               value="{{ $post->h1 }}">
{{--TODO:add some if to value with old()--}}
                    @if ($errors->has('h1'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('h1') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                               value="{{ $post->title }}">
                        @if ($errors->has('title'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text">Slug</label>
                        <input class="form-control" id="slug" name="slug" type="text" placeholder="Slug"
                               value="{{ $post->slug }}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description"
                                  rows="3">{{ $post->description }}</textarea>
                        @if ($errors->has('description'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text">Text</label>
                        <textarea class="form-control" id="text" name="text" rows="8">{{ $post->text }}</textarea>
                        @if ($errors->has('text'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('text') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="author">Author</label>
                        <input class="form-control" id="author" name="author" type="text" placeholder="Author"
                               value="{{ $post->user->name }}" disabled>
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control" id="published_at" name="published_at"
                                   placeholder="YYYY-MM-DD HH:MM:SS" value="{{ $post->published_at }}"/>
                            @if ($errors->has('published_at'))
                                <div class="invalid-feedback d-block">
                                    {{ $errors->first('published_at') }}
                                </div>
                            @endif
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar">
                                </span>
                            </span>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker2').datetimepicker({
                                locale: 'ru'
                            });
                        });
                    </script>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Отложить публикацию(муляж)</label>
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent category</label>
                        <select multiple="multiple" class="selectpicker form-control" id="parent_id" name="parent_id[]"
                                data-live-search="true">
                            @foreach($categories as $category)
                                @foreach($post->categories as $k => $currentCategory)
                                    @if($category->id == $currentCategory->id && $k != count($post->categories)-1)
                                        <option value="{{ $category->id }}"
                                                selected="selected">{{ $category->name }}</option>
                                        @break
                                    @elseif($category->id == $currentCategory->id && $k == count($post->categories)-1)
                                        <option value="{{ $category->id }}"
                                                selected="selected">{{ $category->name }}</option>
                                    @elseif($category->id != $currentCategory->id && $k == count($post->categories)-1)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('parent_id') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.posts.delete', ['id'=>$post->id]) }}" method='POST'>
                    @method('DELETE')
                    @csrf
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
