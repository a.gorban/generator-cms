@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.settings.update') }}" method="POST">
                    @method('PUT')
                    @csrf
                    @foreach($settings as $setting)
                        <div class="form-group">
                            <label for="{{ $setting->part }}">{{ $setting->part }}</label>
                            <select class="form-control" id="{{ $setting->part }}" name="{{ $setting->part }}">
                                @for($i = 1; $i <= $setting->max_value; $i++)
                                    @if($i == $setting->value)
                                        <option value="{{ $i }}" selected="selected">
                                            {{ $i }}
                                        </option>
                                    @else
                                        <option value="{{ $i }}">
                                            {{ $i }}
                                        </option>
                                    @endif
                                @endfor
                            </select>
                        </div>
                    @endforeach
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
