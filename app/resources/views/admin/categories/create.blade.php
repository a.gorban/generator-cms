@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.categories.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Название</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                               value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="h1">H1</label>
                        <input type="text" class="form-control" id="h1" name="h1" placeholder="H1"
                               value="{{ old('h1') }}">
                        @if ($errors->has('h1'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('h1') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                               value="{{ old('title') }}">
                        @if ($errors->has('title'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="5">
                            {{ old('description') }}
                        </textarea>
                        @if ($errors->has('description'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('description') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent category</label>
                        <select class="form-control" id="parent_id" name="parent_id">
                            <option value="0">
                                Главная категория
                            </option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">
                                    {{ $category->name }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('parent_id') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
