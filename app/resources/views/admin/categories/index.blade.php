@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-2">
            <div class="col d-flex align-items-center justify-content-between">
                <h3>Категории</h3>
                <a href="{{route('admin.categories.create')}}">
                    <button class="btn btn-success">Создать Категорию</button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col" class="text-center">Дейстиве</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <th scope="row">{{ $category->id }}</th>
                            <td>{{ $category->name }}</td>
                            <td class="text-center d-flex justify-content-center">
                                <a href="{{ route('admin.categories.edit', ["id"=>$category->id]) }}" class="ml-1 mr-1">
                                    <button type="button" class="btn btn-primary btn-sm">Редактировать</button>
                                </a>
                                <form action="{{ route('admin.categories.delete', ['id'=>$category->id]) }}"
                                      method='POST' class="ml-1 mr-1">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <h3>Категорий нет!</h3>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col">
                {{ $categories->links() }}
            </div>
        </div>

    </div>
@endsection
