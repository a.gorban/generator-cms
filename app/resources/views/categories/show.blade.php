@php
    $title='Lara News- Страница с постами';
    $description='Lara News- Страница с постами';
    $keywords='Новости, Генератор, Суперпроект';
@endphp
@extends('layouts.app')

@section('content')
    <div class="f-row main-row">
        <div class="f-col f-col-lg-8zz">
            <div class="b_header">
                <h1>{{ $category->h1 }}</h1>
            </div>
            <div id="feed" class="b_articles">
                @forelse($currentPosts as $post)
                    <div class="media" data-id="{{ $post->id }}">
                        <div class="media-left">
                            <a href="{{ '/' }}"
                               style="background:rgba(133,146,146,0.6);">
                                <img
                                    data-original="https://picsum.photos/240/180"
                                    class="visible-xs-block lazy" alt="Италия достопримечательности">
                                <img src="https://picsum.photos/240/180"
                                    class="hidden-xs lazy" alt="Италия достопримечательности">
                            </a>

                        </div>


                        <div class="media-body">

                            <h2 class="media-heading">
                                <a href="{{ route('post', ['slug' => $post->slug]) }}">{{ $post->h1 }}</a>
                            </h2>

                            @foreach($post->categories as $category)
                                <a class="label label-main hidden-xs"
                                   href="{{ route('category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                            @endforeach

                            <span class="media_date hidden-xs">{{ $post->published_at }}</span>

                            <p class="hidden-xs">
                                <a href="{{ route('post', ['slug' => $post->slug]) }}">{{ $post->description }}</a>
                            </p>

                        </div>
                    </div>
                @empty
                    <h1>Тут ничего нет</h1>

                @endforelse
                {{ $currentPosts->links() }}

            </div>
        </div>
    </div>
    </div>

@endsection
